const book = require('./MongooseSchema/Book');
const user = require('./MongooseSchema/User');

module.exports = {
    book: book,
    user: user
}

