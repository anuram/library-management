const express = require('express');
const bodyParser = require('body-parser');
const {graphqlHTTP} = require('express-graphql');
const schema = require('./Schema/Schema');
const config = require('./Config/Config');
const loginController = require('./Methods');
const dataController = require('./Methods');
const db = require('./db/db');
const cors =  require('cors');
const multer = require('multer');

const storage = multer.diskStorage({
    destination: (req, file, cb) => {
      cb(null, config.UPLOAD_PATH);
    },
    filename: (req, file, cb) => {
      console.log(file);
      cb(null, file.originalname);
    }
  });
  const upload = multer({
    storage: storage,
    limits: {
      fileSize: 1024 * 1024 * 5
    }
  });

let init = async ()=>{
    await db.dbInit();

    const app = express();
    
    app.use(bodyParser.urlencoded({
        extended: false
    }));
    app.use(cors())

    app.use(bodyParser.json());
    var allowCrossDomain = function (req, res, next) {
        res.header('Access-Control-Allow-Origin', 'http://localhost:4200');
        res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE,PATCH,OPTIONS');
        res.header('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept, Authorization');

        next();
    }
    app.use(allowCrossDomain);
    app.put("/upload", upload.single('image'), function (req, res) {
        console.log('req.file',req.file)
        res.end();
      });
    app.post('/login',loginController.login);
    app.post('/bookList',dataController.addBookList);
    app.delete('/bookList',dataController.removeBookList);
    app.get('/bookList/:id',dataController.getBookList);
    app.use('/graphql',graphqlHTTP({
        schema,
        graphiql:true
    }));

    app.listen(config.PORT,()=>{
        console.log('Server running on port '+config.PORT);
    })
}

init();