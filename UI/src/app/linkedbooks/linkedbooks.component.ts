import { Component} from '@angular/core';
import { GraphQlService } from "../../shared/graphql.service";
import { Http, Response, Headers, URLSearchParams, RequestOptions, RequestMethod } from '@angular/http';
import { environment } from "../../environments/environment";

@Component({
  selector: 'app-linkedbooks',
  templateUrl: './linkedbooks.component.html',
  styleUrls: ['./linkedbooks.component.css']
})
export class LinkedbooksComponent{
  books:any =[]
  filterdBooks: any = []; 
  totalBooks = 0;
  limit = 5;
  page = 0;
  localApiUrl = environment.localApiUrl;

  constructor(
    private graphqlService: GraphQlService,
    private http: Http    
    ) {
  }

  ngOnInit() {
    this.getBooks();
  }

  getBooks(){
    let id =  localStorage.getItem('id')
    this.http.get(this.localApiUrl + "bookList/"+id)
    .map((res: Response) => res.json())
    .subscribe(res=>{
      console.log("res",res)
      console.log(res['result'])
      this.books = res['result']
      this.totalBooks = this.books.length;
      this.onChange(null);

    })
  }

  onChange(event) {
    this.page = event ? event.pageIndex : 0;
    this.limit = event ? event.pageSize : 5;
    this.filterdBooks = this.books.slice(this.page * this.limit, (this.page + 1) * this.limit);
    console.log(this.filterdBooks)
  }

}